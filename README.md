# NishantPodTestFirst

[![CI Status](http://img.shields.io/travis/Nishant Thite/NishantPodTestFirst.svg?style=flat)](https://travis-ci.org/Nishant Thite/NishantPodTestFirst)
[![Version](https://img.shields.io/cocoapods/v/NishantPodTestFirst.svg?style=flat)](http://cocoapods.org/pods/NishantPodTestFirst)
[![License](https://img.shields.io/cocoapods/l/NishantPodTestFirst.svg?style=flat)](http://cocoapods.org/pods/NishantPodTestFirst)
[![Platform](https://img.shields.io/cocoapods/p/NishantPodTestFirst.svg?style=flat)](http://cocoapods.org/pods/NishantPodTestFirst)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NishantPodTestFirst is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "NishantPodTestFirst"
```

## Author

Nishant Thite, Nishant.Thite@globant.com

## License

NishantPodTestFirst is available under the MIT license. See the LICENSE file for more info.
