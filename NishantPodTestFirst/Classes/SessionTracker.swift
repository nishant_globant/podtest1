//
//  SessionTracker.swift
//  Pods
//
//  Created by Nishant Thite on 05/04/17.
//
//

import Foundation

public struct SessionTracker {
    public static func startTracking(_ app: UIApplication) {
        print("\n\n \(app.keyWindow?.rootViewController) \n")
    }
}
