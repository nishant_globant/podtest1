//
//  TelephonyHelper.swift
//  TelephonyTest
//
//  Created by Nishant Thite on 27/03/17.
//  Copyright © 2017 MVP. All rights reserved.
//

import Foundation
import CoreTelephony


public struct TelephonyHelper {
    public static func getTelephonyDetails () -> String {
        
        // 7 - CTTelephonyNetworkInfo
        let newtworkInfo = CTTelephonyNetworkInfo()
        let currentRadioAccessTechnology = (" \n currentRadioAccessTechnology : \(newtworkInfo.currentRadioAccessTechnology) \n")
        
        // 3 - CTCarrier
        let ctCarrier = newtworkInfo.subscriberCellularProvider
        
        let mobileCountryCode = (" \n mobileCountryCode : \(ctCarrier?.mobileCountryCode) \n ")
        let carrierName = (" \n carrierName : \(ctCarrier?.carrierName) \n ")
        let mobileNetworkCode = (" \n mobileNetworkCode : \(ctCarrier?.mobileNetworkCode) \n ")
        let isoCountryCode = (" \n isoCountryCode : \(ctCarrier?.isoCountryCode) \n ")
        let allowsVOIP = (" \n allowsVOIP : \(ctCarrier?.allowsVOIP) \n ")
        
        // 4 - CTCellularData
        let ctCellularData = CTCellularData()
        var cellularDataState = " \n cellularDataState : "
        
        switch ctCellularData.restrictedState {
        case .restrictedStateUnknown :
            cellularDataState = cellularDataState + "restrictedStateUnknown \n"
            break
        case .restricted :
            cellularDataState = cellularDataState + "restricted \n"
            break
        case .notRestricted :
            cellularDataState = cellularDataState + "notRestricted \n"
            break
        }
        
        // 1 - CTCall is deprecated and may not be relevant here
        // 2 - CTCallCenter is deprecated and may not be relevant here
        // 5 - CTSubscriber may not be relevant here
        // 6 - CTSubscriberInfo derives from CTSubscriber and may not be relevant here
        // 8 - CoreTelephonyDefines  may not be relevant here
        
        return mobileCountryCode + carrierName + mobileNetworkCode + isoCountryCode + allowsVOIP + currentRadioAccessTechnology + cellularDataState
    }

}
